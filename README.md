# From classic to cloud

This is the code repository for my [blog about how to get from a classical 3 tier application to a cloud native solution](https://www.itsfullofstars.de/2020/02/from-legacy-to-cloud-native/). The demo solution I developed allows to manage users. It consists of three componentes:

* backend: database, data store.
* middleware: backend, handles data and frontend access.
* frontend: serves the UI app.

Starting with a classical 3 tier solution, where every component is run independently on their own servers, the solution will be transformed to a containerized app and then run on kubernetes (lokal, cloud).

The code is for demo purposes only. It servces to show how you can bring a more complex solution to the cloud. I made the solution to learn how to containerize, what to consider in regards to networking, file storage, etc. In real life, you'd have to consider more parts like security, TLS certificates, etc.

# Architecture

## Backend

A PostgreSQL database.

## Middleware

A Node.JS express app. Provides an API a client can connect to and handles the database access.

## Frontend

An OpenUI5 app to manage users. The app connects to the API of the middleware and can be used to display, add, change and remove users.

# Scenarios

Each scenario can be found in its own branch.